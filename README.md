This is my webpack practice.<br>
https://learn.javascript.ru/screencast/webpack

<h3>Webpack 2.1</h3>
npm i -g webpack<br>
npm i -g webpack-cli<br>
webpack - собрать проект<br>
npm i -g node-static - простой сервер (http://127.0.0.1:8080/home.html)<br>
static & - запустить сервер<br>

<h3>Webpack 2.2</h3>
Вынести модуль в window и запустить отдельно

<h3>Webpack 2.3</h3>
Отслеживание изменений в файлах, таймаут на пересборку

<h3>Webpack 2.4</h3>
Devtool<br>
https://webpack.js.org/configuration/devtool/#devtool<br>
'source-map' - нормально для продакшена<br>
'inline-source-map' - нормально для dev<br>
'cheap-inline-module-source-map' - нормально для dev<br>

<h3>Webpack 2.5</h3>
Окружение<br>
DefinePlugin<br>
NODE_ENV, USER, LANG...<br>

<h3>Webpack 2.6</h3>
babel-loader<br>
npm install babel-loader<br>
npm install babel-loader @babel/core @babel/preset-env webpack<br>
npm install @babel/runtime<br>
npm install @babel/plugin-transform-runtime<br>

<h3>Webpack 2.7</h3>
resolve и resolveLoader<br>
Показать пути и расширения, которые webpack должен искать.<br>

<h3>Webpack 2.8</h3>
UglifyJsPlugin<br>
min js file<br>


