const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const isDev = NODE_ENV == 'development';

module.exports = {
    mode: isDev ? 'development' : 'production',
    entry: './home.js',
    output: {
        filename: 'build.js',
        library: 'home'
    },
    watch: isDev, //отслеживать изменение файлов
    watchOptions: {
        aggregateTimeout: 100 // время ожидания после изменения
    },

    devtool: isDev ? 'cheap-inline-module-source-map' : 'source-map',
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV : JSON.stringify(NODE_ENV),
            LANG: JSON.stringify('ru')
        }),
    ],
    module: {
        rules: [{
            test: /\.js$/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                    plugins: ['@babel/plugin-transform-runtime']
                }
            }
        }],
    },

    // как webpack ищет модули
    resolve: {
        modules: ['node_modules'],
        extensions: [ ' ','.js',]
    },
    resolveLoader: {
        modules: ['node_modules'],
        moduleExtensions: ['*-loader','*'],
        extensions: [ ' ','.js',]
    },
    optimization: {
        minimizer: [new UglifyJsPlugin()]
    }
};
